var listaXicaras = [];

$(document).ready(function() {
  $('#qtdXicaras').on('keydown keyup input', function(e) {
    // Allow: backspace, delete, tab, escape, enter
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
       // Allow: Ctrl+A, Command+A
      (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
       // Allow: home, end, left, right, down, up
      (e.keyCode >= 35 && e.keyCode <= 40)) {
         // Let it happen, don't do anything
         return;
    }
    // Ensure that it is a number and stop the keypress
    if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)) {
      e.preventDefault();
    }
  });
});


function navigateTo(hash) {
  const content = document.getElementById('content');
  switch (hash) {
    case '#home':
      content.innerHTML = `
        <section class="section">
			<div class="divHome">
				<img src="img/principal.png" Alt="Logo Coffee" />
				<input type="text" style="numeric" id="qtdXicaras" placeholder="Digite a quantidade de xícaras" />
				<input type="button" class="buttonFazerPedido" onClick="FazerPedido()" value="Fazer o pedido" />
				
				<input type="button" class="buttonFazerPedido" onClick="ListarPedidos()" value="Listar os pedidos" />
				
				<div class="divlistaXicaras">
					<h2>Lista de pedidos registrados</h2>
					<ul id="integerList">
					  <!-- List items will be dynamically generated -->
					</ul>
				</div>
			</div>
        </section>
      `;
      break;
    case '#about':
      content.innerHTML = `
        <section class="section">
          <h2>História da Coffe Time</h2>
          <p>A Coffee Time é um evento de apreciação de cafés e foi idealizado para aqueles que buscam conhecer mais sobre o apaixonante mundo dos cafés especiais.</p>

			<p>Nosso objetivo é mostrar aos participantes a longa jornada de um café especial, da semente à xícara, desvendando os segredos da produção dos premiados cafés da Fazenda.</p>

			<p>Conteúdo
			- Apresentação do produto;
			- Origens do Café;
			- O Longo caminho do café para se tornar especial (Plantio, manejo, colheita, secagem, beneficiamento, armazenagem, torra e extração);
			- Visita à plantação;
			- Experiência de torra;
			- Ações de sustentabilidade.</p>
        </section>
      `;
      break;
    case '#contact':
      content.innerHTML = `
        <section class="section">
          <h2>Contato</h2>
          <p>Criador: Coffee Time</p>
		  <p>Email: coffee_time@gmail.com</p>
		  <p>Telefone: (61)3585-6245</p>
		  <p>Brasília - Distrito Federal</p>
        </section>
      `;
      break;
    default:
      content.innerHTML = `
        <section class="section">
          <h2>Página não existe</h2>
          <p>Página não existe.</p>
        </section>
      `;
  }
}

function ListarPedidos(){
	var integerList = $('#integerList');
	integerList.empty();
	
	$('.divlistaXicaras').css('display', 'block');
	
	$.each(listaXicaras, function(index, num) {	
		integerList.append('<li>Pedido ' + (index + 1) + ': ' + listaXicaras[index] + '</li>');
	});
}

function FazerPedido()
{
	var qtdXicaras = $('#qtdXicaras').val();
	
	if(qtdXicaras !== "")
	{
		alert('Pedido de "' + qtdXicaras + ' xícaras" realizado com sucesso');
		
		listaXicaras.push(qtdXicaras);
	}
	else
	{
		alert('Erro. Deve ser informado a quantidade de xícaras para fazer o pedido');
	}
	
	$('#qtdXicaras').val('');
}

document.addEventListener('DOMContentLoaded', () => {
  navigateTo('#home');
  
  const navLinks = document.querySelectorAll('nav a');
  navLinks.forEach(link => {
    link.addEventListener('click', event => {
      event.preventDefault();
      const hash = link.getAttribute('href');
      navigateTo(hash);
      history.pushState(null, null, hash);
    });
  });

  window.onpopstate = () => {
    const hash = window.location.hash;
    navigateTo(hash);
  };
});
